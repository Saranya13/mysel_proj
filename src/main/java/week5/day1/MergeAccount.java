package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class MergeAccount extends ProjectSpecificMethod{
	@BeforeClass
	public void setData() {
		testcaseName ="Mergeaccount";
		testDesc ="Desc-mergeaccTC";
		author="Saranya";
		category="Smoke";			
	}
	@Test
	public void mergeaccount() {
	
	WebElement clkacc1 = locateElement("linktext","Accounts");
	click(clkacc1);
	WebElement clkmergeacc = locateElement("linktext","Merge Accounts");
	click(clkmergeacc);
	WebElement clkfromacc = locateElement("xpath","(//img[@alt='Lookup'])[1]");
	click(clkfromacc);
	
	switchToWindow(1);
	WebElement enteraccid = locateElement("name","id");
	type(enteraccid,"10");
	WebElement clkfindaccbutton = locateElement("xpath", "//button[text()='Find Accounts']");
	click(clkfindaccbutton);
	WebElement clkresult1 = locateElement("xpath","(//a[@class='linktext'])[1]");
	click(clkresult1);
	
	switchToWindow(0);

	WebElement clicktoacc = locateElement("xpath","//*[@id='partyIdTo']/following::a");
	click(clicktoacc);
	
	switchToWindow(2);
	
	WebElement enteraccid2 = locateElement("xpath", "//input[@name='id']");
	type(enteraccid2, "12");
	
	WebElement clkfindaccbutton2 = locateElement("xpath", "//button[text()='Find Accounts']");
	click(clkfindaccbutton2);
	
	WebElement clkresult2 = locateElement("xpath", "(//a[@class='linktext'])[1]");
	click(clkresult2);
	
	switchToWindow(0);
	
	WebElement clkmerge = locateElement("xpath", "//a[text()='Merge']");
	click(clkmerge);
	
	acceptAlert();
	
	WebElement clkfindacc = locateElement("xpath", "//a[text()='Find Accounts']");
	click(clkfindacc);
	
	WebElement enteraccid3 = locateElement("xpath", "//input[@name='id']");
	type(enteraccid3, "123");
	
	WebElement clkfindaccbutton3 = locateElement("xpath", "//button[text()='Find Accounts']");
	click(clkfindaccbutton3);
	
	WebElement verifyrecord= locateElement("xpath", "//div[@class='x-paging-info']");
	String text =  verifyrecord.getText();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
}