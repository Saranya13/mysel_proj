package week5.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;
import wdMethods.SeMethods;

public class CreateAccount extends ProjectSpecificMethod {
	
	@BeforeClass
	public void setData() {
		testcaseName ="CreateAccount";
		testDesc ="Desc-createaccTC";
		author="Saranya";
		category="Smoke";			
	}
	@Test
	public void  createAccount() throws InterruptedException{
		WebElement clkacc = locateElement("linktext","Accounts");
		click(clkacc);
		WebElement crtacc = locateElement("linktext","Create Account");
		click(crtacc);
		WebElement entaccname = locateElement("id","accountName");
		type(entaccname,"Gary");
		String accountName = entaccname.getAttribute("value");
		System.out.println(accountName);
		WebElement selectindustry = locateElement("name","industryEnumId");
		selectDropDownUsingText(selectindustry, "Distribution");
		WebElement selectcurrency = locateElement("id","currencyUomId");
		selectDropDownUsingText(selectcurrency, "ALL - Albanian Lek");
		WebElement selectsource = locateElement("id","dataSourceId");
		selectDropDownUsingText(selectsource, "Conference");
		WebElement selectmarketing = locateElement("id","marketingCampaignId");
		selectDropDownUsingText(selectmarketing, "Automobile");
		WebElement entphn = locateElement("id","primaryPhoneNumber");
		type(entphn,"2344545676");
		WebElement entcity = locateElement("id","generalCity");
		type(entcity,"Chennai");
		WebElement entemail = locateElement("id","primaryEmail");
		type(entemail,"rahul@gmail.com");
		WebElement selectcountry = locateElement("id","generalCountryGeoId");
		selectDropDownUsingText(selectcountry, "India");
		Thread.sleep(2000);
		WebElement selectstateorprovince = locateElement("xpath","//select[@name='generalStateProvinceGeoId']");
		selectDropDownUsingText(selectstateorprovince, "TAMILNADU");
		
		WebElement crtaccbut = locateElement("classname","smallSubmit");
		click(crtaccbut);
		
		WebElement captureaccid = locateElement("xpath", "(//span[@class='tabletext'])[3]");
		String text = captureaccid.getText();
		String replaceAll = text.replaceAll("\\D", "");
		System.out.println(replaceAll);
		
		WebElement clkfindacc = locateElement("linktext","Find Accounts");
		click(clkfindacc);
		
		WebElement entaccname2 = locateElement("xpath","(//input[@name='accountName'])[2]");
		type(entaccname2, accountName);
		
		WebElement entaccid = locateElement("xpath","//input[@name='id']");
		type(entaccid,  replaceAll);
		
		WebElement clcfindaccbutton = locateElement("xpath","//button[text()='Find Accounts']");
		click(clcfindaccbutton);
		
		//WebElement eleVerify = locateElement("classname", "x-paging-info");
		//verifyExactText(eleVerify, "No records to display");
		
		String tableData = locateElement("xpath", "//table[@class='x-grid3-row-table']/tbody/tr").getText();
		System.out.print(tableData);
				
	}

}
