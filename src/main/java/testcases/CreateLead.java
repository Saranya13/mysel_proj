package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;
import week6.day2.LearnExcel;

public class CreateLead extends ProjectSpecificMethod{
	@BeforeSuite
	public void setData() {
		testcaseName = "TC01_Create Lead Report";
		testDesc = "Create Lead";
		author = "Saranya";
		category = "Functional Testing";
		filename = "CreateLead";
	}
	
	@Test(dataProvider="getdata" /*invocationCount=2, invocationTimeOut=20000, enabled= true, groups= "smoke"*/)
	//@Test(/*invocationCount = 2*/ groups = "Smoke")
	public void createLead1(String cname, String fname, String lname){
		//login();
		
		WebElement elecreatelead = locateElement("linktext", "Create Lead");
		click(elecreatelead);
		WebElement elecompanyname = locateElement("id", "createLeadForm_companyName");
		type(elecompanyname, cname);
		WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
		type(elefirstname, fname);
		
		WebElement elelastname = locateElement("id", "createLeadForm_lastName");
		type(elelastname, lname);
		
		WebElement elesource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(elesource,"Conference");
		
		WebElement elemarketingcam = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(elemarketingcam,4);
		
		verifyTitle("Create Lead | opentaps CRM");
		
		WebElement eleleadbutton = locateElement("classname", "smallSubmit");
		click(eleleadbutton);
			}
		/*String[][] array = new String[1][3];
		array[0][0]= "Frank";
		array[0][1]= "R";
		array[0][2]= "Amazon";
		
		array[1][0]= "Amy";
		array[1][1]= "S";
		array[1][2]= "TCS";
		*/
		//return array;
		}
	



