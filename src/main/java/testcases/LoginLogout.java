package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class LoginLogout extends SeMethods{
	@Test
	public void login(){
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);
		WebElement elecrm = locateElement("linktext", "CRM/SFA");
		click(elecrm);
		WebElement elecreatelead = locateElement("linktext", "Create Lead");
		click(elecreatelead);
		WebElement elecompanyname = locateElement("id", "createLeadForm_companyName");
		type(elecompanyname, "Amazon");
		WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
		type(elefirstname, "Alex");
		
		WebElement elelastname = locateElement("id", "createLeadForm_lastName");
		type(elelastname, "Robert");
		
		WebElement elesource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(elesource,"Conference");
		
		WebElement elemarketingcam = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(elemarketingcam,4);
		
		boolean title = verifyTitle("Create Lead | opentaps CRM");
		
		
		//WebElement eleleadbutton = locateElement("classname", "smallSubmit");
		//click(eleleadbutton);
		
	}
	
}









