package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import wdMethods.ProjectSpecificMethod;

public class LearnExcel extends ProjectSpecificMethod{

	public static Object[][] excelData(String filename) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook("./data/"+filename+".xlsx");
		//XSSFSheet sheet = workbook.getSheet("CreateLead");
		XSSFSheet sheet = workbook.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();
		System.out.println("Row count is: "+ rowcount);
		short columncount = sheet.getRow(0).getLastCellNum();
		System.out.println("Coulmn count: "+ columncount);
		Object[][] array = new Object[rowcount][columncount];
		for (int i = 1; i <= rowcount ; i++) {
			XSSFRow row = sheet.getRow(i);
		
		for (int j = 0; j < columncount; j++) {
			XSSFCell column = row.getCell(j);
			String stringCellValue = column.getStringCellValue();
			array[i-1][j] = stringCellValue;
			System.out.println(stringCellValue);
		}
		workbook.close();
			}
		return array;
	}
}