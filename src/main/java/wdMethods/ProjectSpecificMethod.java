package wdMethods;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.LearnExcel;

public class ProjectSpecificMethod extends SeMethods{
	
	@DataProvider(name="getdata")  
	public Object[][] excelData() throws IOException {
		return LearnExcel.excelData(filename);
	
	}
	@Parameters ({"browser", "url", "uname", "password"})
	@BeforeMethod
	public void login(String browserName, String URL, String Username, String Password)
	{
	startApp(browserName, URL);
	WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, Username);
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, Password);
	WebElement elelogin = locateElement("classname","decorativeSubmit");
	click(elelogin);
	WebElement elecrm = locateElement("linktext", "CRM/SFA");
	click(elecrm);
}
	
	@AfterMethod
	public void close() {
		closeBrowser();
	}
}