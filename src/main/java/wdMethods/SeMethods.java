package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import reports.BasicReports;
import reports.HtmlReports;

public class SeMethods extends HtmlReports implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodrivers.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			logStep("The Browser "+browser+" Launched Successfully", "pass");
		} catch (WebDriverException e) {
			logStep("The Browser "+browser+" not  Launched Successfully", "fail");
		}
		
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "classname": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case "name": return driver.findElementByName(locValue);
		case "tagname": return driver.findElementByTagName(locValue);
		case "csssector": return driver.findElementByCssSelector(locValue);
		case "partiallinktext": return driver.findElementByPartialLinkText(locValue);
		
		}
		}catch (NoSuchElementException e) {
			System.out.printf("The element with locator "+locator+" not found.","FAIL");
		}catch (WebDriverException e) {
			System.out.printf("Unknown exception occured while finding "+locator+" with value"+locValue,"FAIL");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		driver.findElementById(locValue);
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
		ele.sendKeys(data);
		System.out.println("The Data "+data+" Entered Successfully");
		takeSnap();
		logStep("The data "+data+" entered Successfully", "Pass");
	}catch (InvalidElementStateException e) {
		System.out.printf("The data: "+data+" could not be entered in the field","FAIL");
		logStep("The entered data "+data+" throws InvalidElementStateException", "fail");
	}catch (WebDriverException e) {
		System.out.printf("Unknown exception occured while entering "+data+" in the field"+ele,"FAIL");
		logStep("The entered data "+data+" throws WebdriverException", "fail");
		
	}}
	

	@Override
	public void click(WebElement ele) {
		String text ="";
		try {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
		logStep("The Element "+ele+" clicked Successfully", "Pass");
	}catch (InvalidElementStateException e) {
		System.out.printf("The element: "+text+" could not be clicked","FAIL");
		logStep("The entered text "+text+" throws InvalidElementStateException", "fail");
	}catch (WebDriverException e) {
		System.out.printf("Unknown exception occured while clicking in the field","FAIL");
		logStep("The entered text "+text+" throws WebDriverException", "fail");
	}}
	@Override
	public String getText(WebElement ele) {
		ele.getText();
		System.out.println("Text "+ele+" entered successfully");
		takeSnap();
		return null;
	}

	

	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
		Select st = new Select(ele);  
		st.selectByVisibleText(value);
		System.out.printf("The dropdown is selected with text "+value,"PASS");
		}catch (WebDriverException e) {
			System.err.printf("The element: "+ele+" could not be found.", "FAIL");
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
		Select si = new Select(ele);  
		si.selectByIndex(index);
		}catch (WebDriverException e) {
			System.err.printf("The element: "+ele+" could not be found.", "FAIL");
		} 
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		boolean title;
		String browsertitle = driver.getTitle();
		try {
		if(browsertitle.contains(expectedTitle))
		{
			System.out.println("Title matched");
			return true;
		}
		else
		{
			System.out.println("Title not matched");
		}
		}catch (WebDriverException e) {
			System.out.printf("Unknown exception occured while verifying the title", "FAIL");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			if(getText(ele).equals(expectedText)) {
				System.out.printf("The text: "+getText(ele)+" matches with the value :"+expectedText,"PASS");
			}else {
				System.out.printf("The text "+getText(ele)+" doesn't matches the actual "+expectedText,"FAIL");
			}
		} catch (WebDriverException e) {
			System.out.printf("Unknown exception occured while verifying the Text", "FAIL");
		} 
		}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(getText(ele).contains(expectedText)) {
				System.out.printf("The expected text contains the actual "+expectedText,"PASS");
			}else {
				System.out.printf("The expected text doesn't contain the actual "+expectedText,"FAIL");
			}
		} catch (WebDriverException e) {
			System.out.printf("Unknown exception occured while verifying the Text", "FAIL");
		} 
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		String aText = ele.getAttribute(attribute);
		try {
		if(aText.equals(attribute)) {
			System.out.printf("The expected attribute :"+attribute+" value matches the actual "+value,"PASS");
		}else {
			System.out.printf("The expected attribute :"+attribute+" value does not matches the actual "+value,"FAIL");
		}
	} catch (WebDriverException e) {
		System.out.printf("Unknown exception occured while verifying the Attribute Text", "FAIL");
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String aText = ele.getAttribute(attribute);
		try {
			if(aText.contains(attribute)) {
				System.out.printf("The expected attribute :"+attribute+" value contains the actual ","PASS");
			}else {
				System.out.printf("The expected attribute :"+attribute+" value does not contains the actual ","FAIL");
			}
		} catch (WebDriverException e) {
			System.out.printf("Unknown exception occured while verifying the Attribute Text", "FAIL");
			}
			}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				System.out.printf("The element "+ele+" is selected","PASS");
			} else {
				System.out.printf("The element "+ele+" is not selected","FAIL");
			}
		} catch (WebDriverException e) {
			System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				System.out.printf("The element "+ele+" is displayed","PASS");
			} else {
				System.out.printf("The element "+ele+" is not displayed","FAIL");
			}
		} catch (WebDriverException e) {
			System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
		}
	
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
		Set<String> allWindowHandles = driver.getWindowHandles();
		List<String> allHandles = new ArrayList<>();
		allHandles.addAll(allWindowHandles);
		driver.switchTo().window(allHandles.get(index));
	} catch (NoSuchWindowException e) {
		System.out.printf("The driver could not move to the given window by index "+ index,"PASS");
	} catch (WebDriverException e) {
		System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
	}}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		driver.switchTo().frame(ele);
		System.out.printf("switch In to the Frame "+ele,"PASS");
		} catch (NoSuchFrameException e) {
			System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
		} catch (WebDriverException e) {
			System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
		} 
	}

	@Override
	public void acceptAlert() {
		String text = "";
		try {
		Alert alert = driver.switchTo().alert();
		text = alert.getText();
		alert.accept();
		System.out.printf("The alert "+text+" is accepted", "PASS");
	} catch (NoAlertPresentException e) {
		System.out.printf("There is no alert present.","FAIL");
	} catch (WebDriverException e) {
		System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
	}
	}

	@Override
	public void dismissAlert() {
		String text = "";
		try {
		Alert alert = driver.switchTo().alert();
		text = alert.getText();
		alert.dismiss();
		System.out.printf("The alert "+text+" is dismissed", "PASS");
	} catch (NoAlertPresentException e) {
		System.out.printf("There is no alert present.","FAIL");
	} catch (WebDriverException e) {
		System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
	}
	}

	@Override
	public String getAlertText() {
		String text ="";
		try {
		Alert alert = driver.switchTo().alert();
		text = alert.getText();
		}
		catch (NoAlertPresentException e) {
			System.out.printf("There is no alert present.","FAIL");
		} catch (WebDriverException e) {
			System.out.printf("WebDriverException : "+e.getMessage(), "FAIL");
		} 
		return text;
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
      driver.close();
      System.out.printf("The browser is closed","PASS");
	}catch (Exception e) {
		System.out.printf("The browser could not be closed", "FAIL");
	}
	}

	@Override
	public void closeAllBrowsers() {
		try {
		      driver.quit();
		      System.out.printf("The opened browsers are closed","PASS");
			}catch (Exception e) {
				System.out.printf("Unexpected error occured in Browser", "FAIL");
			}
	}


	@Override
	public String getTitle(WebElement ele) {
		String title = "";
		try {
			title =  driver.getTitle();
		} catch (WebDriverException e) {
			System.out.println("Unknown Exception Occured While fetching Title");
		} 
		return title;
	}


	@Override
	public String getAttribute(WebElement ele) {
		String attribute = "";
		try {
			attribute=  ele.getAttribute(attribute);
		} catch (WebDriverException e) {
			System.out.printf("The element: "+ele+" could not be found.", "FAIL");
		} 
		return attribute;
	}
}