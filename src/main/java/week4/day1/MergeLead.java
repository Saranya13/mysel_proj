package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead{

	public static void main(String[] args) throws InterruptedException {
		
		//Set path
		System.setProperty("webdriver.chrome.driver","D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
		// Create object
		ChromeDriver driver = new ChromeDriver();
		//Pass URL
		driver.get("http://leaftaps.com/opentaps/");
		//Enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter password
		driver.findElementById("password").sendKeys("crmsfa");
		//Click login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click crmsfa, leads, merge leads
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		//Click From lead icon and select 1st result in new window
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> ls = new ArrayList<>();
		ls.addAll(windowHandles);
		driver.switchTo().window(ls.get(1));
		
		//WebDriver window1 = driver.switchTo().window(driver.getWindowHandle());
		//String text2 = driver.findElementByClassName("linktext").getText();
		driver.findElementByName("id").sendKeys("123");
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("(//a[@class='linktext'])[1]")));
		//Click First Resulting lead
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		//Switch back to primary window
		Set<String> wh2 = driver.getWindowHandles();
		List<String> lst2= new ArrayList<>();
		lst2.addAll(wh2);
		driver.switchTo().window(lst2.get(0));
		
		//Click on Icon near To Lead
		driver.findElementByXPath("//*[@id='partyIdTo']/following::a").click();
		
		//Move to new window
		Set<String> wh3 = driver.getWindowHandles();
		List<String> lst3= new ArrayList<>();
		lst3.addAll(wh3);
		driver.switchTo().window(lst3.get(1));

		//Enter Lead ID
		driver.findElementByName("id").sendKeys("112");
		
		//Click Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("(//a[@class='linktext'])[1]")));
		//Click First Resulting lead
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		//Switch back to primary window
				Set<String> wh4 = driver.getWindowHandles();
				List<String> lst4= new ArrayList<>();
				lst4.addAll(wh4);
				driver.switchTo().window(lst4.get(0));
				
				//Click Merge
				driver.findElementByLinkText("Merge").click();
				//Accept Alert
				String alertText = driver.switchTo().alert().getText();
				Thread.sleep(3000);
				System.out.println(alertText);
				driver.switchTo().alert().accept();
				//Click find leads
				driver.findElementByLinkText("Find Leads").click();
				driver.findElementByName("id").sendKeys("1234");
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				Thread.sleep(1000);
				
				//Close browser
				driver.close();
				
				/*
						
				
				
				Click Find Leads
				Enter From Lead ID
				Click Find Leads
				Verify error msg
				Close the browser (Do not log out)*/

		}
		
	}

