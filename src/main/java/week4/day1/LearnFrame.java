package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
		
		//Object creation
		
		ChromeDriver driver = new ChromeDriver();
		//pass url 
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		WebElement frame = driver.findElementByXPath("//button[@onclick='myFunction()']");
		frame.click();
		String printtext = driver.switchTo().alert().getText();
		System.out.println(printtext);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
       //Alert 
		
		driver.switchTo().alert().sendKeys("Saranya");
		driver.switchTo().alert().accept();
		//driver.switchTo().alert().dismiss();
	}
}
