package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class EditLead  extends ProjectSpecificMethod{

	
		// TODO Auto-generated method stub
		@BeforeSuite
		public void setdata() {
			testcaseName = "TC02_EditLead";
			testDesc = "Edit Lead";
			author = "Saranya";
			category = "Functional Testing";
			filename = "EditLead";
		}
	
		//@Test
		@Test(/*groups= "sanity",*/ dataProvider= "getdata", dependsOnMethods = "testcases.CreateLead.createLead1")
		public void editLead(String fname) throws InterruptedException {
			/*startApp("chrome", "http://leaftaps.com/opentaps");
			type(locateElement("username"), "DemoSalesManager");
			type(locateElement("password"), "crmsfa");
			click(locateElement("classname", "decorativeSubmit"));
			click(locateElement("linktext", "CRM/SFA"));
			*/
			
			click(locateElement("linktext", "Leads"));
			click(locateElement("linktext", "Find Leads"));
			type(locateElement("xpath", "(//input[@name = 'firstName'])[3]"), fname);
			click(locateElement("xpath", "//button[text()='Find Leads']"));
			Thread.sleep(3000);
			click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
			verifyTitle("View Lead | opentaps CRM");
			click(locateElement("linktext", "Edit"));
			type(locateElement("id", "updateLeadForm_companyName"), "TEST");
			click(locateElement("classname", "smallSubmit"));
			//verifyExactAttribute(locateElement("viewResponsibleFor_partyId_sp"), "id", "viewResponsibleFor_partyId_sp");	
		}
	}