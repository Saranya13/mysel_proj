package week4.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class DuplicateLead extends ProjectSpecificMethod {

		@BeforeSuite
		public void setData() {
			testcaseName = "TC04_Duplicate Lead";
			testDesc = "Duplicating  the lead ";
			author = "Saranya";
			category = "Smoke";
			filename = "DuplicateLead";
		}

		@Test(dataProvider= "getdata")
		public void duplicateLead(String email) throws InterruptedException {

			// To click on the lead link

			click(locateElement("linktext", "Leads"));

			// To click on the find lead

			click(locateElement("linktext", "Find Leads"));

			// To click on the email tab

			click(locateElement("xpath", "//span[text()='Email']"));

			// To enter email address

			type(locateElement("name", "emailAddress"), email);

			// To click on the find lead button

			click(locateElement("xpath", "//button[text()='Find Leads']"));

			Thread.sleep(500);

			// To capture the name of the first result

			WebElement fname = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]");

			String name = fname.getText();

			System.out.println(name);

			// To click on the first resulting lead

			click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]"));

			// To click on the duplicate lead

			click(locateElement("linkText", "Duplicate Lead"));

			// To get the title and verify

			String title = driver.getTitle();

			if (title.contains("Duplicate Lead")) {

				System.out.println("The corresponding title name is displayed");

			}

			else

			{

				System.out.println("Title name is incorrect");

			}

			// To click on the create lead link

			click(locateElement("name", "submitButton"));

			// To confirm the duplicate name same as captured

			WebElement firstName = locateElement("id", "viewLead_firstName_sp");

			String actualFname = firstName.getText();
			
			System.out.println(actualFname);

			System.out.println(actualFname);

			if (name == actualFname) {

				System.out.println("Duplicate lead name is same as captured");

			}

			else {

				System.out.println("Dupliacte name is not same as captured");

			}

		}

		/*@DataProvider(name = "fetchData")
		public String[][] getData() {

			String[][] data = new String[1][1];

			data[0][0] = "test@gmail.com";
			return data;
*/		}