package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import wdMethods.ProjectSpecificMethod;

public class CreadLeadTC extends ProjectSpecificMethod{

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\\\saran\\\\GradleProject\\\\Workspace\\\\selenium\\\\drivers\\\\chromedriver.exe");

		//Object creation

		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		//clicking Create Lead button
		driver.findElementByLinkText("Create Lead").click();


		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Jack", Keys.TAB);
		driver.findElementById("createLeadForm_lastName").sendKeys("Andrew");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("J", Keys.TAB);
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("A");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Dear");


		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select sc1 = new Select(source);
		//Using List printing all options in console from the drop  down
		List<WebElement> options = sc1.getOptions();

		for (WebElement eachName : options) {
			System.out.println(eachName.getText());
		}
		sc1.selectByIndex(3);

		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Title1");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("300000");

		//without using List just selecting the particular value from the dropdown
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select sc2 = new Select(industry);
		sc2.selectByValue("IND_HEALTH_CARE");

		WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		Select sc3 = new Select(ownership);
		sc3.selectByVisibleText("S-Corporation");

		driver.findElementById("createLeadForm_sicCode").sendKeys("003");
		driver.findElementById("createLeadForm_description").sendKeys("none", Keys.TAB);

		driver.findElementById("createLeadForm_importantNote").sendKeys("none", Keys.TAB);
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("1", Keys.TAB);
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("2");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("002");

		driver.findElementById("createLeadForm_departmentName").sendKeys("New");


		WebElement preferredcurrency = driver.findElementById("createLeadForm_currencyUomId");	
		Select sc4 =new Select(preferredcurrency);
		List<WebElement> options1 = sc4.getOptions();

		for (WebElement eachName: options1) {
			System.out.println(eachName.getText());
		}
		sc4.selectByVisibleText("MRO - Mauritanian Ouguiya");


		driver.findElementById("createLeadForm_numberEmployees").sendKeys("1200");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("TS");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("James");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("url123.com" ,Keys.TAB);

		driver.findElementById("createLeadForm_generalToName").sendKeys("HARRY");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("123 st1" , Keys.TAB);
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("123 st2" , Keys.TAB);
		driver.findElementById("createLeadForm_generalCity").sendKeys("chennai" , Keys.TAB);
		/*	driver.findElementById("createLeadForm_generalStateProvinceGeoId").clear();*/

		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select sc5 =new Select(country);
		sc5.selectByValue("IND");

		WebElement stateprovince = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select sc6 =new Select(stateprovince);
		sc6.selectByValue("IN-TN");

		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600069");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("004");

		WebElement marketingcompaign = driver.findElementById("createLeadForm_marketingCampaignId");
		Select sc7 =new Select(marketingcompaign);
		sc7.selectByValue("CATRQ_AUTOMOBILE");

		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("1478400000");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("jac@j.com");
		driver.findElementByClassName("smallSubmit").click();

	}

}

//Window 
/*String text = driver.findElementById("viewLead_parentPartyId_sp").getText();
		if (text.contains(text2)) {
			System.out.println("Matched");
		} else {
			System.out.println("Not Matched");
 */



