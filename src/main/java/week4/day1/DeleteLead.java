package week4.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;
import wdMethods.SeMethods;

public class DeleteLead extends ProjectSpecificMethod {
	@BeforeSuite
	public void setdata() {
		testcaseName = "TC03_Delete Lead ";
		testDesc = "Delete Lead";
		author = "Saranya";
		category = "Functional Testing";
		filename = "DeleteLead";
	}
	
	@Test (enabled= true, dataProvider="getdata"/*, dependsOnMethods = "testcases.CreateLead.createLead1"*/)
	public void deletelead(String Phnnumber) {
	/*	startApp("chrome", "http://leaftaps.com/opentaps");
		type(locateElement("username"), "DemoSalesManager");
		type(locateElement("password"), "crmsfa");
		click(locateElement("classname", "decorativeSubmit"));
		click(locateElement("linktext", "CRM/SFA"));*/
		
		
	click(locateElement("linktext", "Leads"));
	click(locateElement("linktext", "Find Leads"));
	click(locateElement("xpath", "//span[text()='Phone']"));
	type(locateElement("name", "phoneNumber"), Phnnumber);
	/*click(locateElement("xpath", "//button[text()='Find Leads']"));
	click(locateElement("xpath", "(//a[@class='linktext'])[4]"));
	click(locateElement("classname", "subMenuButtonDangerous"));
	
*/}
}
