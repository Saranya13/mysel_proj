package week4.day1;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class NewMergeLead extends ProjectSpecificMethod{
	@BeforeSuite
	public void setData() {
		testcaseName = "TC05_Merge Lead Report";
		testDesc = "Merge Lead";
		author = "Saranya";
		category = "Functional Testing";
		filename = "MergeLead";
	}
	@Test(/*groups="Regression",*/ enabled = true, dataProvider = "getdata")
	public void MergeLead(String leadid){
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Merge Leads"));
		click(locateElement("xpath", "(//img[@alt='Lookup'])[1]"));
		switchToWindow(1);
		type(locateElement("name", "id"), leadid);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		click(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToWindow(0);
	}		
	
	}
