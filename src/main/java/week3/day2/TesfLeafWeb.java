package week3.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class TesfLeafWeb {

	public static void main(String[] args) throws InterruptedException {
		// set path
		System.setProperty("webdriver.chrome.driver","D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//Pass URL
		driver.get("http://leaftaps.com/opentaps/");
		//Enter username, first name, last name, password and click login, crm/sfa, leads link, find leads and phone 
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Phone").click();
		driver.findElementByName("phoneAreaCode").sendKeys("91");
		driver.findElementByName("phoneNumber").sendKeys("9797979797");
		driver.findElementByLinkText("Find Leads").click();
		
		
		

	}

}
