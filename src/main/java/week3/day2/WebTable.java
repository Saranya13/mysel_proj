package week3.day2;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
		// Create object
		ChromeDriver driver = new ChromeDriver();
		//Pass URL
		driver.get("https://erail.in/");
		driver.manage().window().maximize();//table[@class='DataTable TrainList']
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("CBE",Keys.TAB);
		Thread.sleep(5000);
		//table call
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> row = table.findElements(By.tagName("tr"));
		System.out.println(row.size());
		for (WebElement eachRow : row) {
			
			List<WebElement> column = eachRow.findElements(By.tagName("td"));
			String text = column.get(1).getText();
			System.out.println(text);
		}
	}

}
