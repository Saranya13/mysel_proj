package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class RailWayRegister {

	public static void main(String[] args) throws InterruptedException {
		//set path
		System.setProperty("webdriver.chrome.driver","D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
		//Create object
		ChromeDriver driver= new ChromeDriver();
		//To maximize the browser
		driver.manage().window().maximize();
		//pass URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//Enter UserID
		driver.findElementById("userRegistrationForm:userName").sendKeys("Saranya");
		//Enter password
		driver.findElementById("userRegistrationForm:password").sendKeys("ABCD12");
		//Confirm password
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("ABCD12");
		//To deal with security question drop down
		WebElement question = driver.findElementById("userRegistrationForm:securityQ");
		Select sq = new Select(question);
		sq.selectByVisibleText("What is your pet name?");
		//set answer for security question
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Puppy");
		//select language from drop down
		WebElement language = driver.findElementById("userRegistrationForm:prelan");
		Select sl = new Select(language);
		sl.selectByValue("en");
		//Set first & last name
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Saranya");;
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Somasundaram");
		//To select gender radio button
		driver.findElementById("userRegistrationForm:gender:1").click();
		//To select DOB - day
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select sd = new Select(day);
		sd.selectByIndex(14);
		//To set month
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select sm = new Select(month);
		sm.selectByVisibleText("DEC");
		//To set year
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select sy = new Select(year);
		sy.selectByValue("1995");
		//To select country
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select sc = new Select(country);
		sc.selectByValue("94");
		//To enter pincode
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600056");
		WebElement tab = driver.findElementById("userRegistrationForm:pincode");
		tab.sendKeys(Keys.TAB);	
		Thread.sleep(3000);
		//To submit form
		driver.findElementById("userRegistrationForm:j_idt502").click();
		//Close browser
		driver.close();
	}
}