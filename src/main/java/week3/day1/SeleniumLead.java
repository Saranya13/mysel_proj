package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumLead {

	public static void main(String[] args) {
		//Set path
		System.setProperty("webdriver.chrome.driver","D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
		// Create object
		ChromeDriver lead = new ChromeDriver();
		//Pass URL
		lead.get("http://leaftaps.com/opentaps/");
		//Enter username
		lead.findElementById("username").sendKeys("DemoSalesManager");
		//Enter password
		lead.findElementById("password").sendKeys("crmsfa");
		//Click login
		lead.findElementByClassName("decorativeSubmit").click();
		//Click crm
		lead.findElementByLinkText("CRM/SFA").click();
		lead.findElementByLinkText("Create Lead").click();
		//Pass values
		lead.findElementById("createLeadForm_companyName").sendKeys("CTS");
		lead.findElementById("createLeadForm_firstName").sendKeys("Saranya");
		lead.findElementById("createLeadForm_lastName").sendKeys("S");
		lead.findElementByClassName("smallSubmit");
		//Select visible for drop-down
		WebElement source = lead.findElementById("createLeadForm_dataSourceId");
		Select sc = new Select(source);
		sc.selectByVisibleText("Public Relations");
		//Select 
		WebElement industry = lead.findElementById("createLeadForm_industryEnumId");
		Select sc1 = new Select(industry);
		sc1.selectByValue("IND_ETAILER");
		//
		WebElement marketingcampaign = lead.findElementById("createLeadForm_marketingCampaignId");
		Select sc2 = new Select(marketingcampaign);
		List<WebElement> options = sc2.getOptions();
				for (WebElement each : options) {
					System.out.println(each.getText());
				}
		sc2.selectByIndex(3);
		
		
		//Window 
		
		
	}
}
