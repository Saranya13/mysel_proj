package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTCRekha {

	public static void main(String[] args) throws InterruptedException {
		//set path
				System.setProperty("webdriver.chrome.driver", "D:\\saran\\GradleProject\\Workspace\\selenium\\drivers\\chromedriver.exe");
				
				//Object creation
				
				ChromeDriver driver = new ChromeDriver();
				//pass url 
				
				driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
				
				//to maximize chrome browser
				driver.manage().window().maximize();

				//get values for username,password 
				driver.findElementById("userRegistrationForm:userName").sendKeys("NewUser");
				driver.findElementById("userRegistrationForm:password").sendKeys("Pass");
				driver.findElementById("userRegistrationForm:confpasword").sendKeys("Pass");
				
				//Select value from the dropdown using select class
				
			WebElement securityquestion = driver.findElementById("userRegistrationForm:securityQ");
			Select sc1 = new Select(securityquestion);
			sc1.selectByVisibleText("What is your pet name?");
			
			driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("lucky");
			driver.findElementById("userRegistrationForm:firstName").sendKeys("Rekha");
			driver.findElementById("userRegistrationForm:middleName").sendKeys("J");
			driver.findElementById("userRegistrationForm:lastName").sendKeys("J");
			
			//Select value from the dropdown using select class
			WebElement dob = driver.findElementById("userRegistrationForm:dobDay");
			Select sc2 =new Select(dob);
			sc2.selectByValue("13");
			
			WebElement dobmonth = driver.findElementById("userRegistrationForm:dobMonth");
			Select sc3 =new Select(dobmonth);
			sc3.selectByValue("04");
			
			WebElement dobyear = driver.findElementById("userRegistrationForm:dateOfBirth");
			Select sc4 =new Select(dobyear);
			sc4.selectByIndex(5);
			
			WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
			Select sc5 =new Select(occupation);
			sc5.selectByVisibleText("Professional");
			
			/*List <WebElement> alloptions = sc5.getOptions();
			int count = alloptions.size();
			sc5.selectByIndex(count-2);*/
			
			driver.findElementById("userRegistrationForm:uidno").sendKeys("123123123");
			driver.findElementById("userRegistrationForm:idno").sendKeys("BGT12365IU");
			
			WebElement country = driver.findElementById("userRegistrationForm:countries");
			Select sc6 =new Select(country);
			sc6.selectByValue("94");
			driver.findElementById("userRegistrationForm:email").sendKeys("new12@gmail.com");
			driver.findElementById("userRegistrationForm:mobile").sendKeys("0111111111");
			
			WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
			Select sc7 =new Select(nationality);
			sc7.selectByIndex(94);
			
			driver.findElementById("userRegistrationForm:address").sendKeys("No: 3/16");
			driver.findElementById("userRegistrationForm:street").sendKeys("Selvam nagar");
			driver.findElementById("userRegistrationForm:area").sendKeys("chennai");
			driver.findElementById("userRegistrationForm:pincode").sendKeys("600069");
			
			//prefill value based on zipcode using TAB key functionality
			WebElement tab = driver.findElementById("userRegistrationForm:pincode");
			tab.sendKeys(Keys.TAB);
			Thread.sleep(3000);
			
			driver.findElementById("userRegistrationForm:landline").sendKeys("0041256987845");
			
			//closing the browser
			driver.close();
			
	}

}
