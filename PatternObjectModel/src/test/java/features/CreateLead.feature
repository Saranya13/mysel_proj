Feature: Create Lead 
#
#Background: 
##	Given Open the Browser 
##	And Max the Browser 
##	And Set the TimeOut 
##	And Launch the URL 

Scenario Outline: Create Login 
	And Enter the UserName as DemoSalesManager
	And Enter the Password as crmsfa 
	When Click on the Login Button 
	When Click on the CRMSFA link
	When Click on the CreateLead button 
	And Enter CompanyName as <Cname>
	And Enter FirstName as <Fname> 
	And Enter LastName as <Lname> 
	When Click on submit
	
	Examples: 
		|Cname|Fname|Lname|
		|Amazon|Dany|J|
		|TCS|Jack|D|
		
Scenario Outline: Edit Login 
	When Click on the CreateLead button 
	And Enter CompanyName as <Cname> 
	And Enter FirstName as <Fname> 
	And Enter LastName as <Lname> 
	When Click on submit 
	
	Examples: 
		|Cname|Fname|Lname|
		|Amazon|Dany|J|
		|TCS|Jack|D|