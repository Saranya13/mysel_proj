package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
@Before
	public void before(Scenario sc)
	{
	startResult();
	startTestModule(sc.getName(), sc.getId());
	test = startTestCase(sc.getName());
	test.assignCategory("Functional");
	test.assignAuthor("Saranya");
	startApp("chrome", "http://leaftaps.com/opentaps");		

	/*System.out.println(sc.getName());
		System.out.println(sc.getId());
	*/}
@After
	public void after(Scenario sc)
	{
	closeAllBrowsers();
	endResult();
	
	/*System.out.println(sc.getStatus());
		System.out.println(sc.isFailed());*/
	}
}
