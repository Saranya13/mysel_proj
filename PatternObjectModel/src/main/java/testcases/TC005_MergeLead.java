package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testDescription = "MergeLead";
		authors = "saranya";
		category = "smoke";
		dataSheetName = "TC005";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void createlead(String username, String password, String id)
	{
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickmergelead()
		.fromleadicon();
		/*.enterleadid(id)
		.clickmergefirstleadresult();*/
}
}