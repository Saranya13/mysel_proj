package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_DeleteLead";
		testDescription = "DeleteLead";
		authors = "saranya";
		category = "smoke";
		dataSheetName = "TC004";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void createlead(String username, String password, String phonenumber)
	{
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickfindlead()
		.phonetab()
		.phonenumber(phonenumber)
		.findleadbutton()
		.clickfirstlead()
		.clickdelete();		
}
}