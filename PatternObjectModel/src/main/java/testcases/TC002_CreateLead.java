package testcases;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.HomePage;
import pages.LoginPage;
import pages.MyHomePage;
import pages.MyLeadsPage;
import pages.ViewLeadPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "saranya";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void createlead(String username, String password, String companyname,String firstname, String lastname)
	{
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickcreatelead()
		.enterCompanyname(companyname)
		.enterfirstname(firstname)
		.enterlastname(lastname)
		.clickcreatelead();
		
	}
	
}
