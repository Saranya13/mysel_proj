package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLead";
		testDescription = "EditLead";
		authors = "saranya";
		category = "smoke";
		dataSheetName = "TC003";
		testNodes = "Leads";
	}
	@Test(dataProvider="fetchData")
	public void createlead(String username, String password, String firstname, String companyname)
	{
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clickLead()
		.clickfindlead()
		.enterfirstname(firstname)
		.findleadbutton()
		.clickfirstlead()
		//.Verifypagetitle(data);
		.clickedit()
		.editcompanyname(companyname)
		.clickupdate();
	}
}