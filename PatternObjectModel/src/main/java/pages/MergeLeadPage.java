package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{
	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}
		
		@FindBy (how=How.XPATH, using = "//img[@alt='Lookup'])[1]")WebElement eleclickfromleadicon;
		public MergeLeadPage fromleadicon() {
			click(eleclickfromleadicon);
			switchToWindow(1);
			return this;
					
		}
		
		@FindBy (how=How.XPATH, using="(//img[@alt='Lookup'])[2]") WebElement eleclicktoleadicon;
		public MergeLeadPage toleadicon() {
			click(eleclicktoleadicon);
			switchToWindow(1);
			return this;
		}
		
		
		/*@FindBy (how=How.XPATH, using = "//img[@alt='Lookup'])[1]")WebElement eleclickfromleadicon;
		public void fromleadicon() {
			click(eleclickfromleadicon);
			//return new findleadspage;
		}
		*/
}
