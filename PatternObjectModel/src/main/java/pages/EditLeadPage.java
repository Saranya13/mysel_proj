package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleeditcompname;
	public EditLeadPage editcompanyname(String data) {
		type(eleeditcompname, data);//9952286274 9952286274
		return this;
	}

	@FindBy(how=How.XPATH, using="(//input[@class='smallSubmit'])[1]") WebElement eleclickupdatebutton;
	public ViewLeadPage clickupdate() {
		click(eleclickupdatebutton);
		return new ViewLeadPage();
	}
	
}

