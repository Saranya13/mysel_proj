package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName") WebElement eleverification;
	public void Verifycompanyname(String data) {
	
		verifyExactText(eleverification, data);
		System.out.println(eleverification);	
	}
	
	@FindBy(how=How.CLASS_NAME,using="subMenuButtonDangerous") WebElement eleclickdeletebutton;
	public MyLeadsPage clickdelete() {
		click(eleclickdeletebutton);
		return new MyLeadsPage();
		
	}
	@FindBy(id="sectionHeaderTitle_leads") WebElement eleverifypagetitle;
	public void Verifypagetitle(String data) {
	
		verifyExactText(eleverifypagetitle, data);
		System.out.println(eleverifypagetitle);	
	}
	
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleclickeditbutton;
	public EditLeadPage clickedit() {
		click(eleclickeditbutton);
		return new EditLeadPage();	
	}
	
}
