package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using= "createLeadForm_companyName")WebElement elecompanyname;
	@And ("Enter CompanyName as (.*)")
	public CreateLeadPage enterCompanyname(String companyname) {
		type(elecompanyname, companyname);
		return this;
	}

	
	@FindBy(how = How.ID, using= "createLeadForm_firstName")WebElement elefirstname;
	@And("Enter FirstName as (.*)")  
	public CreateLeadPage enterfirstname (String data) {
		type(elefirstname, data );
		return this;
	}
	
	@FindBy(how = How.ID, using= "createLeadForm_lastName")WebElement elelastname;
	@And("Enter LastName as (.*)") 
	public CreateLeadPage enterlastname (String data) {
		type(elelastname, data );
		return this;
	}
	
	@FindBy(how = How.CLASS_NAME, using="smallSubmit") WebElement elecreateleadbutclk;
	@And("Click on submit")
		public ViewLeadPage clickcreatelead() {
		click(elecreateleadbutclk);
		return new ViewLeadPage() ;
	}
}
