package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using= "Create Lead")WebElement eleclickcreatelead;
	@When ("Click on the CreateLead button")
	public CreateLeadPage clickcreatelead() {
		click(eleclickcreatelead);
		return new CreateLeadPage();
		//return new
	}
	@FindBy(how = How.LINK_TEXT, using= "Merge Leads")WebElement eleclickmergelead;
	public MergeLeadPage clickmergelead() {
		click(eleclickmergelead);
		return new MergeLeadPage();
	
	}
	
	@FindBy(how = How.LINK_TEXT, using= "Find Leads")WebElement eleclickfindlead;
	public FindLeadsPage clickfindlead() {
		click(eleclickfindlead);
		return new FindLeadsPage();
	
	}

}
	
	
