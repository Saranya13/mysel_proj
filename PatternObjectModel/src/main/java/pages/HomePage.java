package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using= "CRM/SFA")WebElement elecrmsfaclick;
	@When ("Click on the CRMSFA link")
	public MyHomePage clickcrmsfa() {
		click(elecrmsfaclick);
		return new MyHomePage();
	}

	
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleLogout;

	public LoginPage clickLogout() {		
		click(eleLogout);
		return new LoginPage();
	}








}
