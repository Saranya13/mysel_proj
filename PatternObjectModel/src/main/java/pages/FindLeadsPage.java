package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
		public FindLeadsPage() {
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(how=How.XPATH, using="//span[text()='Phone']") WebElement clickphonetab;
		public FindLeadsPage phonetab() {
			click(clickphonetab);
			return this;
		}
		
		@FindBy(how=How.XPATH, using="//input[@name='phoneNumber']") WebElement enterphonenumber;
		public FindLeadsPage phonenumber(String data) {
			type(enterphonenumber, data);//9952286274 9952286274
			return this;
		}
		
		@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement clickfindleadbutton;
		public FindLeadsPage findleadbutton() {
			click(clickfindleadbutton);
			return this;
		}
		
		@FindBy(how=How.XPATH, using="(//a[@class='linktext'])[4]") WebElement clickfirstleadresult;
		public ViewLeadPage clickfirstlead() {
			click(clickfirstleadresult);
			return new ViewLeadPage();
		}
		@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement elefindfirstname;
		public FindLeadsPage enterfirstname(String data) {
			type(elefindfirstname, data);//9952286274 9952286274
			return this;
		}
		
		@FindBy(how=How.NAME, using="id") WebElement eleenterleadid;
		public FindLeadsPage enterleadid(String data) {
			type(eleenterleadid, data);
			return this;
		}
		
		@FindBy(how=How.XPATH, using="(//a[@class='linktext'])[1]") WebElement eleclkmergefstleadresult;
		public MergeLeadPage clickmergefirstleadresult() {
			click(eleclkmergefstleadresult);
			switchToWindow(0);
			return new MergeLeadPage();
		
		}
}